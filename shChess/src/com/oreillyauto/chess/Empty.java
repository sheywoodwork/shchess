package com.oreillyauto.chess;

import java.awt.Point;

public class Empty extends ChessPiece {

    public static int empties = 0;

    public Empty() {
        this.type = ChessPiece.EMPTY;
        this.color = ChessPiece.EMPTY;
        empties += 1;
    }

    public Point getStart() {
        int x, y;
        if (empties > 8) {
            if (empties > 16) {
                if (empties > 24) {
                    y = 6;
                    x = empties - 24;
                } else {
                    y = 5;
                    x = empties - 16;
                }
            } else {
                y = 4;
                x = empties - 8;
            }
        } else {
            y = 3;
            x = empties;
        }
        return new Point(x, y);
    }

    @Override
    protected void finalize() {
        empties -= 1;
    }

    public static boolean isEmpty(Point p, Board board) {
        if ((board.get(p).color.equals("-")) && (board.get(p).type.equals("-"))) {
            return true;
        } else {
            return false;
        }
    }
}
