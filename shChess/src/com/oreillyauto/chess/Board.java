package com.oreillyauto.chess;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Board {

    private Map<Point, ChessPiece> board;

    public Map<Point, ChessPiece> getBoard() {
        return board;
    }
    
    public static List<String> xAxis = new ArrayList<String>(
            Arrays.asList(" ", "a", "b", "c", "d", "e", "f", "g", "h"));

    private int turn = 1;

    public int getTurn() {
        return turn;
    }

    public void nextTurn() {
        this.turn += 1;
    }
    
    public static void clearTheConsole() {
        try {
            Thread.sleep(500);
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void buildChessBoard() {
        int i;
        board = new HashMap<Point, ChessPiece>();
        for (i = 0; i < 8; i++) {
            Pawn pawn = new Pawn(ChessPiece.COLOR_WHITE);
            board.put(pawn.getStart(), pawn);
        }
        for (i = 0; i < 8; i++) {
            Pawn pawn = new Pawn(ChessPiece.COLOR_BLACK);
            board.put(pawn.getStart(), pawn);
        }

        for (i = 0; i < 2; i++) {
            Rook rook = new Rook(ChessPiece.COLOR_WHITE);
            board.put(rook.getStart(), rook);
        }
        for (i = 0; i < 2; i++) {
            Rook rook = new Rook(ChessPiece.COLOR_BLACK);
            board.put(rook.getStart(), rook);
        }

        for (i = 0; i < 2; i++) {
            Night night = new Night(ChessPiece.COLOR_WHITE);
            board.put(night.getStart(), night);
        }
        for (i = 0; i < 2; i++) {
            Night night = new Night(ChessPiece.COLOR_BLACK);
            board.put(night.getStart(), night);
        }

        for (i = 0; i < 2; i++) {
            Bishop bishop = new Bishop(ChessPiece.COLOR_WHITE);
            board.put(bishop.getStart(), bishop);
        }
        for (i = 0; i < 2; i++) {
            Bishop bishop = new Bishop(ChessPiece.COLOR_BLACK);
            board.put(bishop.getStart(), bishop);
        }

        King wKing = new King(ChessPiece.COLOR_WHITE);
        board.put(wKing.getStart(), wKing);

        King bKing = new King(ChessPiece.COLOR_BLACK);
        board.put(bKing.getStart(), bKing);

        Queen wQueen = new Queen(ChessPiece.COLOR_WHITE);
        board.put(wQueen.getStart(), wQueen);

        Queen bQueen = new Queen(ChessPiece.COLOR_BLACK);
        board.put(bQueen.getStart(), bQueen);

        for (i = 0; i < 32; i++) {
            Empty empty = new Empty();
            board.put(empty.getStart(), empty);
        }
    }

    public void displayChessBoard() {
        System.out.println("    a  b  c  d  e  f  g  h");
        for (int y = 8; y > 0; y--) {
            System.out.print(" " + y + " ");
            for (int x = 1; x < 9; x++) {
                Point point = new Point(x, y);
                System.out.print(board.get(point).getAbbreviation() + " ");
            }
            System.out.print(y);
            System.out.println("");
        }
        System.out.println("    a  b  c  d  e  f  g  h");
    }

    public boolean onBoard(Point p) {
        return board.containsKey(p);
    }

    public boolean isOpen(Point p) {
        if (board.get(p).getAbbreviation().contentEquals("--")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean canEnter(Point p, String color) {
        if (board.get(p).getColor().equals(color)) {
            return false;
        } else {
            return true;
        }
    }

    public String getColor(Point p) {
        return board.get(p).getColor();
    }

    public ChessPiece get(Point p) {
        return board.get(p);
    }

    public ChessPiece put(Point p, ChessPiece cp) {
        return board.put(p, cp);
    }

    public void placeOnBoard(Point p, ChessPiece cp) {
        board.put(p, cp);
    }

    public void putAll(Map<Point, ChessPiece> m) {
        board.putAll(m);
    }

    public void promote(Point p, Scanner in) {
        System.out.println("");
        System.out.println(" " + this.get(p).getFullName() + " is evolving! Please pick a promotion:");
        System.out.println(" 1. Queen");
        System.out.println(" 2. Knight");
        System.out.println(" 3. Bishop");
        System.out.println(" 4. Rook");
        String color = this.getColor(p);
        int temp = in.nextInt();
        Pawn.notPromote = false;
        switch (temp) {
            case 1:
                clearTheConsole();
                System.out.println(" " + this.get(p).getFullName() + " at " + pointToChessLocation(p) + " has been promoted to a Queen");
                System.out.println("");
                this.get(p).finalize();
                this.put(p, new Queen(color));
                break;
            case 2:
                clearTheConsole();
                System.out.println(" " + this.get(p).getFullName() + " at " + pointToChessLocation(p) + " has been promoted to a Knight");
                System.out.println("");
                this.get(p).finalize();
                this.put(p, new Night(color));
                break;
            case 3:
                clearTheConsole();
                System.out.println(" " + this.get(p).getFullName() + " at " + pointToChessLocation(p) + " has been promoted to a Bishop");
                System.out.println("");
                this.get(p).finalize();
                this.put(p, new Bishop(color));
                break;
            case 4:
                clearTheConsole();
                System.out.println(" " + this.get(p).getFullName() + " at " + pointToChessLocation(p) + " has been promoted to a Rook");
                System.out.println("");
                this.get(p).finalize();
                this.put(p, new Rook(color));
                break;
            default:
                System.out.println(" Not a valid option");
                System.out.println("");
                promote(p, in);
                break;

        }
        Pawn.notPromote = true;
        if (in.hasNextLine()) {
            in.nextLine();
        }

    }
    
    public static String pointToChessLocation(Point p) {
        String x = Board.xAxis.get(p.x);
        return x + p.y;
    }

}
