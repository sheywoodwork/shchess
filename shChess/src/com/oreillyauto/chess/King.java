package com.oreillyauto.chess;

import java.awt.Point;
import java.util.Map;

public class King extends ChessPiece {

    public King(String color) {
        this.type = ChessPiece.TYPE_KING;
        this.color = color;
        if (color == ChessPiece.COLOR_BLACK) {
            blk += 1;
        } else {
            wht += 1;
        }
    }

    public static int blk = 0;
    public static int wht = 0;
    public static Point blkLocation = new Point(5, 8);
    public static Point whtLocation = new Point(5, 1);
    public static int blkMoved = 0;
    public static int whtMoved = 0;

    @Override
    protected void finalize() {
        if (this.color == ChessPiece.COLOR_BLACK) {
            blk -= 1;
        } else {
            wht -= 1;
        }
        System.out.println(" " + this.getFullName() + " was destroyed");
        System.out.println("");
    }

    public Point getStart() {
        int x, y;
        if (color == ChessPiece.COLOR_BLACK) {
            y = 8;
            x = 5;
        } else {
            y = 1;
            x = 5;
        }
        return new Point(x, y);
    }

    @Override
    public String movePiece(Point start, Point end, Board board) {
        if (board.get(start).isValid(start, end, board)) {
            if (start.equals(new Point(5, 8))) {
                blkMoved = 1;
            }
            if (start.equals(new Point(5, 1))) {
                whtMoved = 1;
            }
            System.out.println(" " + board.get(start).getFullName() + " at " + Board.pointToChessLocation(start) + " moved to " + Board.pointToChessLocation(end));
            System.out.println("");
            board.get(end).finalize();
            board.put(end, board.get(start));
            board.put(start, new Empty());
            if (board.get(end).getColor().equals("B")) {
                blkLocation = end;
            } else {
                whtLocation = end;
            }
            return "Success";
        } else {
            return "Failed";
        }
    }

    @Override
    public boolean isValid(Point start, Point end, Board board) {
        if ((end.equals(new Point(3, 1))) || (end.equals(new Point(7, 1))) || (end.equals(new Point(3, 8)))
                || (end.equals(new Point(7, 8)))) {
            if (tryCastle(start, end, board)) {
                return true;
            }
        }

        if ((start.getX() + 1 >= end.getX()) && start.getY() + 1 >= end.getY() && (start.getX() - 1 <= end.getX())
                && start.getY() - 1 <= end.getY()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkCheck(Point at, Board board) {
        for (Map.Entry<Point, ChessPiece> entry : board.getBoard().entrySet()) {
            if (entry.getKey().equals(at)) {
                continue;
            } else {
                if ((entry.getValue().isValid(entry.getKey(), at, board)) && (board.canEnter(at, entry.getValue().color))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean checkCheck(Point at, Board board, ChessPiece cp) {
        ChessPiece temp = board.get(at);
        board.put(at, cp);
        for (Map.Entry<Point, ChessPiece> entry : board.getBoard().entrySet()) {
            if (entry.getKey().equals(at)) {
                continue;
            } else {
                if ((entry.getValue().isValid(entry.getKey(), at, board)) && (board.canEnter(at, entry.getValue().color))) {
                    board.put(at, temp);
                    return true;
                }
            }
        }
        board.put(at, temp);
        return false;
    }

    public static boolean tryCastle(Point start, Point end, Board board) {
//        System.out.println("trying castle");//debugging
        if ((board.getColor(start).equals("W")) && (King.whtMoved == 0) && (!King.checkCheck(start, board, board.get(start)))) {
//            System.out.println("stage 1-1");//debugging
            if ((end.equals(new Point(3, 1))) && (Rook.whtLeftMoved == 0) && (board.get(new Point(1, 1)).getColor().equals("W"))
                    && (board.get(new Point(1, 1)).getType().equals("R")) && (Empty.isEmpty(new Point(4, 1), board))
                    && (Empty.isEmpty(new Point(3, 1), board)) && (Empty.isEmpty(new Point(2, 1), board))
                    && (!King.checkCheck(new Point(4, 1), board, board.get(start)))
                    && (!King.checkCheck(new Point(3, 1), board, board.get(start)))) {
//                System.out.println("stage 1-2");//debugging
                board.put(new Point(4, 1), board.get(new Point(1, 1)));
                board.put(new Point(1, 1), new Empty());
                Rook.whtLeftMoved = 1;
                System.out.println(" White castled long");
                System.out.println(" White Rook at a1 moved to d1");
                return true;
            }
            if ((end.equals(new Point(7, 1))) && (Rook.whtRightMoved == 0) && (board.get(new Point(8, 1)).getColor().equals("W"))
                    && (board.get(new Point(8, 1)).getType().equals("R")) && (Empty.isEmpty(new Point(6, 1), board))
                    && (Empty.isEmpty(new Point(7, 1), board)) && (!King.checkCheck(new Point(6, 1), board, board.get(start)))
                    && (!King.checkCheck(new Point(7, 1), board, board.get(start)))) {
//                System.out.println("stage 1-3");//debugging
                board.put(new Point(6, 1), board.get(new Point(8, 1)));
                board.put(new Point(8, 1), new Empty());
                Rook.whtRightMoved = 1;
                System.out.println(" White castled short");
                System.out.println(" White Rook at h1 moved to f1");
                return true;
            }
        }
        if ((board.getColor(start).equals("B")) && (King.blkMoved == 0) && (!King.checkCheck(start, board, board.get(start)))) {
//            System.out.println("stage 2-1");//debugging
            if ((end.equals(new Point(3, 8))) && (Rook.blkLeftMoved == 0) && (board.get(new Point(1, 8)).getColor().equals("B"))
                    && (board.get(new Point(1, 8)).getType().equals("R")) && (Empty.isEmpty(new Point(4, 8), board))
                    && (Empty.isEmpty(new Point(3, 8), board)) && (Empty.isEmpty(new Point(2, 8), board))
                    && (!King.checkCheck(new Point(4, 8), board, board.get(start)))
                    && (!King.checkCheck(new Point(3, 8), board, board.get(start)))) {
//                System.out.println("stage 2-2");//debugging
                board.put(new Point(4, 8), board.get(new Point(1, 8)));
                board.put(new Point(1, 8), new Empty());
                Rook.blkLeftMoved = 1;
                System.out.println(" Black castled long");
                System.out.println(" Black Rook at a8 moved to d8");
                return true;
            }
            if ((end.equals(new Point(7, 8))) && (Rook.blkRightMoved == 0) && (board.get(new Point(8, 8)).getColor().equals("B"))
                    && (board.get(new Point(8, 8)).getType().equals("R")) && (Empty.isEmpty(new Point(6, 8), board))
                    && (Empty.isEmpty(new Point(7, 8), board)) && (!King.checkCheck(new Point(6, 8), board, board.get(start)))
                    && (!King.checkCheck(new Point(7, 8), board, board.get(start)))) {
//                System.out.println("stage 2-3"); //debugging
                board.put(new Point(6, 8), board.get(new Point(8, 8)));
                board.put(new Point(8, 8), new Empty());
                Rook.blkRightMoved = 1;
                System.out.println(" Black castled short");
                System.out.println(" Black Rook at h8 moved to f8");
                return true;
            }
        }
        return false;
    }
}
