package com.oreillyauto.chess;

import java.awt.Point;

public class Rook extends ChessPiece {

    public Rook(String color) {
        this.type = ChessPiece.TYPE_ROOK;
        this.color = color;
        if (color == ChessPiece.COLOR_BLACK) {
            blk += 1;
        } else {
            wht += 1;
        }
    }

    public static int blk = 0;
    public static int wht = 0;
    public static int blkRightMoved = 0;
    public static int whtRightMoved = 0;
    public static int blkLeftMoved = 0;
    public static int whtLeftMoved = 0;

    @Override
    protected void finalize() {
        if (this.color == ChessPiece.COLOR_BLACK) {
            blk -= 1;
        } else {
            wht -= 1;
        }
        System.out.println(" " + this.getFullName() + " was destroyed");
        System.out.println("");
    }

    public Point getStart() {
        int x, y;
        if (color == ChessPiece.COLOR_BLACK) {
            y = 8;
            x = (blk == 1) ? 1 : 8;
        } else {
            y = 1;
            x = (wht == 1) ? 1 : 8;
        }
        return new Point(x, y);
    }

    @Override
    public String movePiece(Point start, Point end, Board board) {
        if (board.get(start).isValid(start, end, board)) {
            if (start.equals(new Point(1, 8))) {
                blkLeftMoved = 1;
            }
            if (start.equals(new Point(1, 1))) {
                whtLeftMoved = 1;
            }
            if (start.equals(new Point(8, 8))) {
                blkRightMoved = 1;
            }
            if (start.equals(new Point(8, 1))) {
                whtRightMoved = 1;
            }
            System.out.println(" " + board.get(start).getFullName() + " at " + Board.pointToChessLocation(start) + " moved to " + Board.pointToChessLocation(end));
            System.out.println("");
            board.get(end).finalize();
            board.put(end, board.get(start));
            board.put(start, new Empty());
            return "Success";
        } else {
            return "Failed";
        }
    }

    @Override
    public boolean isValid(Point start, Point end, Board board) {
        if (start.getX() == end.getX()) {
            int big = Math.max(start.y, end.y);
            int small = Math.min(start.y, end.y);
            for (int i = 1; i < big - small; i++) {
                if (!board.isOpen(new Point(start.x, big - i))) {
                    return false;
                }
            }
            return true;
        } else if (start.getY() == end.getY()) {
            int big = Math.max(start.x, end.x);
            int small = Math.min(start.x, end.x);
            for (int i = 1; i < big - small; i++) {
                if (!board.isOpen(new Point(big - i, start.y))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}