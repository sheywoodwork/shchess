package com.oreillyauto.chess;

import java.awt.Point;

public class Pawn extends ChessPiece {

    public static int blk = 0;
    public static int wht = 0;
    public static int en = 0;
    public static Point passant = null;
    public static boolean notPromote = true;

    public Pawn(String color) {
        this.type = ChessPiece.TYPE_PAWN;
        this.color = color;
        if (color == ChessPiece.COLOR_BLACK) {
            blk += 1;
        } else {
            wht += 1;
        }
    }

    @Override
    protected void finalize() {
        if (this.color == ChessPiece.COLOR_BLACK) {
            blk -= 1;
        } else {
            wht -= 1;
        }
        if (notPromote) {
            System.out.println(" " + this.getFullName() + " was destroyed");
            System.out.println("");
        }
    }

    public Point getStart() {
        int x, y;
        if (color == ChessPiece.COLOR_BLACK) {
            y = 7;
            x = blk;
        } else {
            y = 2;
            x = wht;
        }
        return new Point(x, y);
    }

    @Override
    public String movePiece(Point start, Point end, Board board) {
        if (board.get(start).isValid(start, end, board)) {
            System.out.println(" " + board.get(start).getFullName() + " at " + Board.pointToChessLocation(start) + " moved to " + Board.pointToChessLocation(end));
            System.out.println("");
            board.get(end).finalize();
            board.put(end, board.get(start));
            board.put(start, new Empty());
            return "Success";
        } else {
            return "Failed";
        }
    }

    @Override
    public boolean isValid(Point start, Point end, Board board) {
        if (board.getColor(start).equals("B")) {
            if (board.isOpen(end)) {
                if (start.getY() == 7) {
                    if ((start.getY() - 1 == end.getY()) && (start.getX() == end.getX())) {
                        return true;
                    } else if ((start.getY() - 2 == end.getY()) && (start.getX() == end.getX())
                            && (board.isOpen(new Point(end.x, start.y - 1)))) {
                        Pawn.en = board.getTurn();
                        Pawn.passant = end;
                        return true;
                    }
                } else {
                    if ((start.getY() - 1 == end.getY()) && (start.getX() == end.getX())) {
                        return true;
                    }
                }
            } else if (board.getColor(end).equals("W")) {
                if ((start.getY() == end.getY() + 1) && ((start.getX() == end.getX() - 1) || (start.getX() == end.getX() + 1))) {
                    return true;
                }
                if ((start.getY() == 4) && (board.get(end).type.equals("P"))) {
                    if ((start.getY() == end.getY()) && ((start.getX() == end.getX() - 1) || (start.getX() == end.getX() + 1))
                            && (Pawn.en == board.getTurn() - 1) && (Pawn.passant.equals(end))) {
                        return true;
                    }
                }
            }
        }

        if (board.getColor(start).equals("W")) {
            if (board.isOpen(end)) {
                if (start.getY() == 2) {
                    if ((start.getY() + 1 == end.getY()) && (start.getX() == end.getX())) {
                        return true;
                    } else if ((start.getY() + 2 == end.getY()) && (start.getX() == end.getX())
                            && (board.isOpen(new Point(end.x, start.y + 1)))) {
                        Pawn.en = board.getTurn();
                        Pawn.passant = end;
                        return true;
                    }
                } else {
                    if ((start.getY() + 1 == end.getY()) && (start.getX() == end.getX())) {
                        return true;
                    }
                }
            } else if (board.getColor(end).equals("B")) {
                if ((start.getY() == end.getY() - 1) && ((start.getX() == end.getX() - 1) || (start.getX() == end.getX() + 1))) {
                    return true;
                }
                if ((start.getY() == 5) && (board.get(end).type.equals("P"))) {
                    if ((start.getY() == end.getY()) && ((start.getX() == end.getX() - 1) || (start.getX() == end.getX() + 1))
                            && (Pawn.en == board.getTurn() - 1) && (Pawn.passant.equals(end))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
