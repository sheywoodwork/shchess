package com.oreillyauto.chess;

import java.awt.Point;

/**
 * ChessPiece abbreviations should be one of the following:
 *   BR - Black Rook        WR - White Rook
 *   BN - Black Knight      WN - White Knight
 *   BB - Black Bishop      WB - White Bishop
 *   BK - Black King        WK - White King
 *   BQ - Black Queen       WQ - White Queen
 *   BP - Black Pawn        WP - White Pawn
 * @author jbrannon5
 */
public class ChessPiece {

    public final static String COLOR_BLACK = "B";
    public final static String COLOR_WHITE = "W";
    public final static String TYPE_KING = "K";
    public final static String TYPE_QUEEN = "Q";
    public final static String TYPE_BISHOP = "B";
    public final static String TYPE_KNIGHT = "N";
    public final static String TYPE_ROOK = "R";
    public final static String TYPE_PAWN = "P";  
    public final static String EMPTY = "-";
    public int en = 0;
    public static int blk;
    public static int wht;
    
    protected String type = "";
    protected String color = "";
    
    public ChessPiece() {
    }

    public ChessPiece(String type, String color) {
        this.type = type;
        this.color = color;
    }
    
    @Override
    protected void finalize(){
        if (this.color == ChessPiece.COLOR_BLACK) {
            blk -= 1;
        } else {
            wht-= 1;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getAbbreviation() {
        return this.color + this.type;
    }

    public String movePiece(Point start, Point end, Board board) {
        // TODO Auto-generated method stub
        return null; 
    }

    public boolean isValid(Point start, Point end, Board board) {
        // TODO Auto-generated method stub
        return false;
    }
    
    public String getFullName() {
        String tempType = new String();
        String tempColor = (this.color.equals("W")) ? "White" : (this.color.equals("B"))? "Black" : "Empty";
        switch (this.type) {
            case "P":
                tempType = "Pawn";
                break;
            case "K":
                tempType = "King";
                break;
            case "Q":
                tempType = "Queen";
                break;
            case "B":
                tempType = "Bishop";
                break;
            case "N":
                tempType = "Knight";
                break;
            case "R":
                tempType = "Rook";
                break;
            default: 
                tempType = "Square";
                break;
        }
        String full = tempColor + " " + tempType;
        return full;
    }

}
