package com.oreillyauto.chess;

import java.awt.Point;

public class Queen extends ChessPiece {

    public Queen(String color) {
        this.type = ChessPiece.TYPE_QUEEN;
        this.color = color;
        if (color == ChessPiece.COLOR_BLACK) {
            blk += 1;
        } else {
            wht += 1;
        }
    }

    public static int blk = 0;
    public static int wht = 0;

    @Override
    protected void finalize() {
        if (this.color == ChessPiece.COLOR_BLACK) {
            blk -= 1;
        } else {
            wht -= 1;
        }
        System.out.println(" " + this.getFullName() + " was destroyed");
        System.out.println("");
    }

    public Point getStart() {
        int x, y;
        if (color == ChessPiece.COLOR_BLACK) {
            y = 8;
            x = 4;
        } else {
            y = 1;
            x = 4;
        }
        return new Point(x, y);
    }

    @Override
    public String movePiece(Point start, Point end, Board board) {
        if (board.get(start).isValid(start, end, board)) {
            System.out.println(" " + board.get(start).getFullName() + " at " + Board.pointToChessLocation(start) + " moved to " + Board.pointToChessLocation(end));
            System.out.println("");
            board.get(end).finalize();
            board.put(end, board.get(start));
            board.put(start, new Empty());
            return "Success";
        } else {
            return "Failed";
        }
    }

    @Override
    public boolean isValid(Point start, Point end, Board board) {
        if (Math.abs((start.getX() - end.getX())) == Math.abs(start.getY() - end.getY())) {
            if ((Integer.signum(start.x - end.x)) == (Integer.signum(start.y - end.y))) {
                if (start.x > end.x) {
                    for (int i = 1; i < start.x - end.x; i++) {
                        if (!board.isOpen(new Point(end.x + i, end.y + i))) {
                            return false;
                        }
                    }
                } else {
                    for (int i = 1; i < end.x - start.x; i++) {
                        if (!board.isOpen(new Point(start.x + i, start.y + i))) {
                            return false;
                        }
                    }
                }
            } else {
                if (start.x > end.x) {
                    for (int i = 1; i < start.x - end.x; i++) {
                        if (!board.isOpen(new Point(end.x + i, end.y - i))) {
                            return false;
                        }
                    }
                } else {
                    for (int i = 1; i < end.x - start.x; i++) {
                        if (!board.isOpen(new Point(start.x + i, start.y - i))) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        if (start.getX() == end.getX()) {
            int big = Math.max(start.y, end.y);
            int small = Math.min(start.y, end.y);
            for (int i = 1; i < big - small; i++) {
                if (!board.isOpen(new Point(start.x, big - i))) {
                    return false;
                }
            }
            return true;
        } else if (start.getY() == end.getY()) {
            int big = Math.max(start.x, end.x);
            int small = Math.min(start.x, end.x);
            for (int i = 1; i < big - small; i++) {
                if (!board.isOpen(new Point(big - i, start.y))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
