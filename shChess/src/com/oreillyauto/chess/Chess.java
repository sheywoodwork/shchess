package com.oreillyauto.chess;

import java.awt.Point;
import java.util.Scanner;

public class Chess {

    public static void main(String[] args) {
        String player = "W";
        boolean gameOver = false;
        Board.clearTheConsole();

        Board board = new Board();
        StringBuilder announce = new StringBuilder();

        board.buildChessBoard();
        Scanner input = new Scanner(System.in);

        game: while (!gameOver) {
            board.displayChessBoard();
            if ((King.blk == 0) || (King.wht == 0)) {
                System.out.println("");
                System.out.println(" The Game is Over!");
                announce.replace(0, announce.length(), ((King.blk == 0) ? " WHITE WON" : " BLACK WON"));
                System.out.println(announce);
                gameOver = true;
                continue;
            }

            for (int i = 1; i < 9; i++) {
                if (board.get(new Point(i, 1)).type.equals("P")) {
                    board.promote(new Point(i, 1), input);
                    continue game;
                }
                if (board.get(new Point(i, 8)).type.equals("P")) {
                    board.promote(new Point(i, 8), input);
                    continue game;
                }
            }

            //            if (King.checkCheck(King.blkLocation, board)) { //debugging
            //                System.out.println("Black is in check");
            //            }
            //            if (King.checkCheck(King.whtLocation, board)) { //debugging
            //                System.out.println("White is in check");
            //            }
            announce.replace(0, announce.length(), (player.equals("W")) ? " WHITE'S TURN" : " BLACK'S TURN");
            System.out.println("");
            System.out.println(announce);
            System.out.println("");
            //            System.out.println("Black location: " + King.blkLocation);  //debugging
            //            System.out.println("White location: " + King.whtLocation);  //debugging
            //            System.out.println("Pawns : " + Pawn.blk + " - " + Pawn.wht);  //debugging
            //            System.out.println("Kings : " + King.blk + " - " + King.wht);  //debugging
            //            System.out.println("Queens : " + Queen.blk + " - " + Queen.wht);  //debugging
            //            System.out.println("Bishops : " + Bishop.blk + " - " + Bishop.wht);  //debugging
            //            System.out.println("Knights : " + Night.blk + " - " + Night.wht);  //debugging
            //            System.out.println("Rooks : " + Rook.blk + " - " + Rook.wht);  //debugging
            //            System.out.println("BLK Rooks moved : " + Rook.blkRightMoved + " - " + Rook.blkLeftMoved);  //debugging
            //            System.out.println("WHT Rooks moved : " + Rook.whtRightMoved + " - " + Rook.whtLeftMoved);  //debugging
            //            System.out.println("Kings moved: " + King.blkMoved + " - " + King.whtMoved);  //debugging

            Point start = new Point();
            System.out.println(" Enter the position of the piece you wish to move: ");
            System.out.print(" ");
            if (input.hasNext("[a-h][1-8]")) {
                String temp = new String();
                temp = input.next();
                int y = Integer.parseInt("" + temp.charAt(1));
                int x = Board.xAxis.indexOf("" + temp.charAt(0));
                start = new Point(x, y);
            } else {
                Board.clearTheConsole();
                System.out.println(" Incorrect input");
                System.out.println("");
                if (input.hasNextLine()) {
                    input.nextLine();
                }
                if (input.hasNext()) {
                    input.next();
                }
                continue;
            }

            if (!board.onBoard(start)) {
                Board.clearTheConsole();
                System.out.println(" That position is not on the board");
                System.out.println("");
                continue;
            }

            if (!player.equals(board.getColor(start))) {
                Board.clearTheConsole();
                System.out.println(" That is not your color");
                System.out.println("");
                continue;
            }

            Point end = new Point();
            System.out.println(" Enter the position of the position you wish to move it to: ");
            System.out.print(" ");
            if (input.hasNext("[a-h][1-8]")) {
                String temp = new String();
                temp = input.next();
                int y = Integer.parseInt("" + temp.charAt(1));
                int x = Board.xAxis.indexOf("" + temp.charAt(0));
                end = new Point(x, y);
            } else {
                Board.clearTheConsole();
                System.out.println(" Incorrect input");
                System.out.println("");
                input.nextLine();
                continue;
            }

            if (start.equals(end)) {
                Board.clearTheConsole();
                System.out.println(" You must move a piece");
                System.out.println("");
                continue;
            }
            Board.clearTheConsole();
            if (board.onBoard(end)) {
                if (board.canEnter(end, player)) {

                    if (board.get(start).movePiece(start, end, board).equals("Failed")) {
                        System.out.println(" That is not a valid move");
                        System.out.println("");
                    } else {
                        player = (player.contentEquals("W")) ? "B" : "W";
                        board.nextTurn();
                    }

                } else {
                    System.out.println(" That position is blocked");
                    System.out.println("");
                }
            } else {
                System.out.println(" That position is not on the board");
                System.out.println("");
            }
        }
        input.close();

    }

}
