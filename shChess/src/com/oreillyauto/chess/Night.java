package com.oreillyauto.chess;

import java.awt.Point;

public class Night extends ChessPiece {

    public Night(String color) {
        this.type = ChessPiece.TYPE_KNIGHT;
        this.color = color;
        if (color == ChessPiece.COLOR_BLACK) {
            blk += 1;
        } else {
            wht += 1;
        }
    }

    public static int blk = 0;
    public static int wht = 0;

    @Override
    protected void finalize() {
        if (this.color == ChessPiece.COLOR_BLACK) {
            blk -= 1;
        } else {
            wht -= 1;
        }
        System.out.println(" " + this.getFullName() + " was destroyed");
        System.out.println("");
    }

    public Point getStart() {
        int x, y;
        if (color == ChessPiece.COLOR_BLACK) {
            y = 8;
            x = (blk == 1) ? 2 : 7;
        } else {
            y = 1;
            x = (wht == 1) ? 2 : 7;
        }
        return new Point(x, y);
    }

    @Override
    public String movePiece(Point start, Point end, Board board) {
        if (board.get(start).isValid(start, end, board)) {
            System.out.println(" " + board.get(start).getFullName() + " at " + Board.pointToChessLocation(start) + " moved to " + Board.pointToChessLocation(end));
            System.out.println("");
            board.get(end).finalize();
            board.put(end, board.get(start));
            board.put(start, new Empty());
            return "Success";
        } else {
            return "Failed";
        }
    }

    @Override
    public boolean isValid(Point start, Point end, Board board) {
        if (((start.getX() + 2 == end.getX()) && ((start.getY() - 1 == end.getY()) || (start.getY() + 1 == end.getY())))
                || ((start.getX() - 2 == end.getX()) && ((start.getY() - 1 == end.getY()) || (start.getY() + 1 == end.getY())))
                || ((start.getY() + 2 == end.getY()) && ((start.getX() - 1 == end.getX()) || (start.getX() + 1 == end.getX())))
                || ((start.getY() - 2 == end.getY()) && ((start.getX() - 1 == end.getX()) || (start.getX() + 1 == end.getX())))) {
            return true;
        } else {
            return false;
        }
    }
}
